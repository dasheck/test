/**********************************************************************************

Copyright (C) 2009  RPdev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

//*********************************************************************************/

#if !defined(__gamesettings_h__)
#define __gamesettings_h__

//*********************************************************************************/
#include "interface.h"

//*********************************************************************************/
#define settings	GameSettings::getInstance()

//*********************************************************************************/
class GameSettings
{
public:
	stringc player1Name, player2Name;
	int gamemode;

private:
	GameSettings(void) {}
	GameSettings(const GameSettings&) {}
	~GameSettings(void) {}

	GameSettings operator =(const GameSettings&);

public:
	static GameSettings& getInstance(void)
	{
		static GameSettings instance;
		return(instance);
	}
};

//*********************************************************************************/
#endif