/**********************************************************************************

Copyright (C) 2009  RPdev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

//************************************************************************/

//************************************************************************/
#include "GameObjectSunshine.h"

//************************************************************************/
GameObjectSunshine::GameObjectSunshine(void)
{

}

//************************************************************************/
GameObjectSunshine::~GameObjectSunshine(void)
{

}

//************************************************************************/
void GameObjectSunshine::newRound(void)
{
	field->initialize();

	for(unsigned int i = 0; i < 2; i++)
	{
		player[i]->newRound();
	}

	numOfChips = 0;
}

//************************************************************************/
bool GameObjectSunshine::proceedInput(const SEvent &event)
{
	if(event.EventType == EET_MOUSE_INPUT_EVENT)
	{
		position2d<int> mouse = position2d<int>(event.MouseInput.X, event.MouseInput.Y);

		if(event.MouseInput.Event == EMIE_MOUSE_MOVED)
		{
			if(rect<int>((irrlichtBase.getWidth()-64*GAME_FIELD_X)/2, (irrlichtBase.getHeight()-64*GAME_FIELD_Y)/2,
				(irrlichtBase.getWidth()+64*GAME_FIELD_X)/2, (irrlichtBase.getHeight()+64*GAME_FIELD_Y)/2).isPointInside(mouse))
			{
				forecastChipPosition = position2d<int>(64*((mouse.X - (irrlichtBase.getWidth()-64*GAME_FIELD_X)/2)/64)+(irrlichtBase.getWidth()-64*GAME_FIELD_X)/2, (irrlichtBase.getHeight()-64*GAME_FIELD_Y)/2-32);

			}
		}

		if(event.MouseInput.Event == EMIE_LMOUSE_PRESSED_DOWN)
		{
			if(rect<int>((irrlichtBase.getWidth()-64*GAME_FIELD_X)/2, (irrlichtBase.getHeight()-64*GAME_FIELD_Y)/2,
				(irrlichtBase.getWidth()+64*GAME_FIELD_X)/2, (irrlichtBase.getHeight()+64*GAME_FIELD_Y)/2).isPointInside(mouse))
			{
				position2d<int> turn = field->doTurn((mouse.X - (irrlichtBase.getWidth()-64*GAME_FIELD_X)/2)/64, player[activePlayer]->getID());
				GameEvent* temp = new GameEvent;
				temp->type = CHIP_SET;
				temp->chipSetEvent.x = turn.X;
				temp->chipSetEvent.y = turn.Y;
				temp->chipSetEvent.player = player[activePlayer]->getID();
				OnGameEvent(temp);			
			}
		}

		if(event.MouseInput.Event == EMIE_RMOUSE_PRESSED_DOWN)
		{
			if(rect<int>((irrlichtBase.getWidth()-64*GAME_FIELD_X)/2, (irrlichtBase.getHeight()-64*GAME_FIELD_Y)/2,
				(irrlichtBase.getWidth()+64*GAME_FIELD_X)/2, (irrlichtBase.getHeight()+64*GAME_FIELD_Y)/2).isPointInside(mouse))
			{
				position2d<int> turn = position2d<int>((mouse.X - (irrlichtBase.getWidth()-64*GAME_FIELD_X)/2)/64, 
					(mouse.Y - (irrlichtBase.getHeight()-64*GAME_FIELD_Y)/2)/64);

				if(field->getPlayer(turn.X,turn.Y) != 0)
				{
					if(field->getPlayer(turn.X,turn.Y) != player[activePlayer]->getID())
					{
						GameEvent* temp = new GameEvent;
						temp->type = CHIP_STEAL;
						temp->chipStealEvent.x = turn.X;
						temp->chipStealEvent.y = turn.Y;
						OnGameEvent(temp);
					}
				}
			}
		}
	}

	return(false);
}


//************************************************************************/
void GameObjectSunshine::OnGameEvent(GameEvent* event)
{
	if(event->type == CHIP_SET)
	{
		if(event->chipSetEvent.y != -1)
		{
			numOfChips++;

			for(unsigned int i = 0; i < 2; i++)
			{
				player[i]->noticeTurn(position2d<int>(event->chipSetEvent.x, event->chipSetEvent.y), player[activePlayer]->getID());
			}

			activePlayer++;
			forecastChip = resource->getItem("ichip64green");

			if(activePlayer > 1)
			{
				activePlayer = 0;
				forecastChip = resource->getItem("ichip64orange");
			}

			if(field->isWinner() != 0)
			{
				GameEvent* temp = new GameEvent;
				temp->type = PLAYER_WON;
				temp->playerWonEvent.player = field->isWinner();
				OnGameEvent(temp);
			}

			if(numOfChips >= GAME_FIELD_X*GAME_FIELD_Y)
			{
				newRound();
			}
		}	
	}

	if(event->type == CHIP_STEAL)
	{
		numOfChips--;
		field->stealChip(event->chipStealEvent.x, event->chipStealEvent.y);

		player[0]->setField(field);
		player[1]->setField(field);

		activePlayer++;
		forecastChip = resource->getItem("ichip64green");

		if(activePlayer > 1)
		{
			activePlayer = 0;
			forecastChip = resource->getItem("ichip64orange");
		}

		if(field->isWinner() != 0)
		{
			GameEvent* temp = new GameEvent;
			temp->type = PLAYER_WON;
			temp->playerWonEvent.player = field->isWinner();
			OnGameEvent(temp);
		}		
	}

	if(event->type == PLAYER_WON)
	{
		if(event->playerWonEvent.player == player[0]->getID())
		{
			player[0]->win();
			player[1]->loose();
			newRound();
		}

		if(event->playerWonEvent.player == player[1]->getID())
		{
			player[1]->win();
			player[0]->loose();
			newRound();
		}
	}
}