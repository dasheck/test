/**********************************************************************************

Copyright (C) 2009  RPdev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

//*********************************************************************************/

//*********************************************************************************/
#include "MainMenu.h"

//*********************************************************************************/
MainMenu::MainMenu(void)
{
	name = "MainMenu";
}

//*********************************************************************************/
MainMenu::~MainMenu(void)
{

}

//*********************************************************************************/
void MainMenu::OnEnter(void)
{
	if(firstEnter)
	{
		background = guidev->addImage(rect<int>(0,0,irrlichtBase.getWidth(),irrlichtBase.getHeight()));
		background->setScaleImage(true);
		background->setImage(resource->getItem("imainbackground"));

		firstEnter = false;

		start = new MinButton;
		start->id = 0;
		start->image = resource->getItem("bstart");
		start->imageHover = resource->getItem("bstart_hover");
		start->position = position2d<int>(irrlichtBase.getWidth()/100.0f*8.0f,irrlichtBase.getHeight()/100.0f*60.83333);

		option = new MinButton;
		option->id = 0;
		option->image = resource->getItem("boptionen");
		option->imageHover = resource->getItem("boptionen_hover");
		option->position = position2d<int>(irrlichtBase.getWidth()/100.0f*20.25f,irrlichtBase.getHeight()/100.0f*73.16667);

		end = new MinButton;
		end->id = 0;
		end->image = resource->getItem("bbeenden");
		end->imageHover = resource->getItem("bbeenden_hover");
		end->position = position2d<int>(irrlichtBase.getWidth()/100.0f*36.25,irrlichtBase.getHeight()/100.0f*85.5);
	}
}

//*********************************************************************************/
void MainMenu::OnLeave(void)
{

}

//*********************************************************************************/
bool MainMenu::OnInput(const SEvent &event)
{
	if(event.EventType == EET_GUI_EVENT)
	{
		unsigned int ID = event.GUIEvent.Caller->getID();

		if(event.GUIEvent.EventType == EGET_BUTTON_CLICKED)
		{
		}
	}

	if(event.EventType == EET_MOUSE_INPUT_EVENT)
	{
		if(event.MouseInput.Event == EMIE_MOUSE_MOVED)
		{
			start->getHoverInformation(position2d<int>(event.MouseInput.X, event.MouseInput.Y));
			option->getHoverInformation(position2d<int>(event.MouseInput.X, event.MouseInput.Y));
			end->getHoverInformation(position2d<int>(event.MouseInput.X, event.MouseInput.Y));
		}

		if(event.MouseInput.Event == EMIE_LMOUSE_PRESSED_DOWN)
		{
			if(start->isHovered)
			{
				irrlichtBase.changeGameState("Game");
			}

			if(option->isHovered)
			{
				irrlichtBase.changeGameState("Options");
			}

			if(end->isHovered)
			{
				device->closeDevice();
			}
		}
	}

	return(false);
}

//*********************************************************************************/
void MainMenu::update(void)
{
	background->draw();

	driver->draw2DImage(start->getImage(), start->position, 
		rect<int>(position2d<int>(0,0), dimension2d<int>(256,64)), 0, SColor(255,255,255,255), true);
	driver->draw2DImage(option->getImage(), option->position, 
		rect<int>(position2d<int>(0,0), dimension2d<int>(256,64)), 0, SColor(255,255,255,255), true);
	driver->draw2DImage(end->getImage(), end->position, 
		rect<int>(position2d<int>(0,0), dimension2d<int>(256,64)), 0, SColor(255,255,255,255), true);
}