/**********************************************************************************

Copyright (C) 2009  RPdev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

//*********************************************************************************/

//*********************************************************************************/
#include "Intro.h"

//*********************************************************************************/
Intro::Intro(void)
{
	name = "Intro";
}

//*********************************************************************************/
Intro::~Intro(void)
{

}

//*********************************************************************************/
void Intro::OnEnter(void)
{
	if(firstEnter)
	{
		background = guidev->addImage(rect<int>(0,0,irrlichtBase.getWidth(),irrlichtBase.getHeight()));
		background->setScaleImage(true);
		background->setImage(resource->getItem("intro_rpdev"));

		firstEnter = false;
	}

	timeStamp = device->getTimer()->getTime();
}

//*********************************************************************************/
void Intro::OnLeave(void)
{

}

//*********************************************************************************/
bool Intro::OnInput(const SEvent &event)
{
	if(event.EventType == EET_GUI_EVENT)
	{
		unsigned int ID = event.GUIEvent.Caller->getID();

		if(event.GUIEvent.EventType == EGET_BUTTON_CLICKED)
		{
			
		}
	}

	if(event.EventType == EET_KEY_INPUT_EVENT)
	{
		if(event.KeyInput.PressedDown)
		{
			if(event.KeyInput.Key == KEY_ESCAPE)
			{
				irrlichtBase.changeGameState("MainMenu");
			}
		}
	}

	return(false);
}

//*********************************************************************************/
void Intro::update(void)
{
	if(device->getTimer()->getTime() - timeStamp > 5000)
	{
		irrlichtBase.changeGameState("MainMenu");
	}

	background->draw();
}