/**********************************************************************************

Copyright (C) 2009  RPdev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

//*********************************************************************************/

#if !defined(__aiplayer_h__)
#define __aiplayer_h__

//*********************************************************************************/
#include "interface.h"
#include "defines.h"
#include "Field.h"

#include <time.h>

//*********************************************************************************/
class AIPlayer : public IAIPlayer
{
private:
	IField* current;
	int playerID;
	int index;

public:
	AIPlayer(void);
	virtual ~AIPlayer(void);
	
	void setCurrentField(IField* field);

	int getRowOfTrust(int player, unsigned int maxDepth, IField* cField);
	int getEvaluation(IField* field, int player);

private:
	int getMax(int depth, IField* currentField, int player);
	int getMin(int depth, IField* currentField, int player);
};

//*********************************************************************************/
#endif