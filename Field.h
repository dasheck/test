/**********************************************************************************

Copyright (C) 2009  RPdev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

//*********************************************************************************/

//*********************************************************************************/
#if !defined(__field_h__)
#define __field_h__

//*********************************************************************************/
#include "interface.h"
#include "defines.h"

//*********************************************************************************/
class Field : public IField
{
private:
	int field[GAME_FIELD_X][GAME_FIELD_Y];

public:
	Field(void);
	~Field(void);

	void initialize(void);
	void shutdown(void);

	vector<int> getField(void);
	void setFieldTo(vector<int> copy);

	void reduceWinnerChips(void);

	position2d<int> doTurn(int row, int player);
	bool isTurnPossible(int row);

	int getPlayer(int indexX, int indexY);
	void stealChip(int indexX, int indexY);

	int isWinner(void);

	bool threeInARowWin(int player);
	bool threeInARow(int player);
	bool twoInARow(int player);
};

//*********************************************************************************/
#endif