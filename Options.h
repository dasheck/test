/**********************************************************************************

Copyright (C) 2009  RPdev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

//*********************************************************************************/

//*********************************************************************************/
#if !defined(__options_h__)
#define __options_h__

//*********************************************************************************/
#include "../GameCreator/GameState.h"
#include "../GameCreator/utility.h"
#include "../GameCreator/defines.h"
#include "interface.h"
#include "GameSettings.h"

//*********************************************************************************/
class Options : public GameState
{
private:
	IGUIImage* background;
	IGUITabControl* root;

	IGUITab* game,*video,*audio,*highscore,*credits;
	
	IGUIButton* fastGame;
	IGUIButton* tetrisGame;
	IGUIButton* sunshineGame;
	unsigned int chosenButton;
	IGUIEditBox* player1Name,*player2Name;
	IGUICheckBox* player1Mode,*player2Mode;
	IGUIStaticText* gameDescription;
	stringw desc[3];

	IGUIComboBox* resolution;
	IGUICheckBox* fullscreen;
	IGUIComboBox* language;

public:
	Options(void);
	~Options(void);

	void OnEnter(void);
	void OnLeave(void);

	bool OnInput(const SEvent &event);
	void update(void);
};

//*********************************************************************************/
#endif