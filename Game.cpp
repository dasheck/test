/**********************************************************************************

Copyright (C) 2009  RPdev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

//*********************************************************************************/

//*********************************************************************************/
#include "Game.h"

//*********************************************************************************/
Game::Game(void)
{
	name = "Game";
}

//*********************************************************************************/
Game::~Game(void)
{

}

//*********************************************************************************/
void Game::OnEnter(void)
{
	if(firstEnter)
	{
		background = guidev->addImage(rect<int>(0,0,irrlichtBase.getWidth(),irrlichtBase.getHeight()));
		background->setScaleImage(true);
		background->setImage(resource->getItem("ibackground"));

		pattern = resource->getItem("ipattern64");
		transwindow = resource->getItem("itranswindow");
		firstEnter = false;

		message = new ScreenMessage;
	}

	switch(settings.gamemode)
	{
		case 0:
			game = new GameObject;
			break;

		case 1:
			game = new GameObjectTetris;
			break;

		case 2:
			game = new GameObjectSunshine;
			break;

		default:
			game = new GameObject;
	}	

	game->initialize();
	timeStamp = device->getTimer()->getTime();
}

//*********************************************************************************/
void Game::OnLeave(void)
{
	game->shutdown();
}

//*********************************************************************************/
bool Game::OnInput(const SEvent &event)
{
	if(event.EventType == EET_GUI_EVENT)
	{
		unsigned int ID = event.GUIEvent.Caller->getID();

		if(event.GUIEvent.EventType == EGET_BUTTON_CLICKED)
		{

		}
	}

	if(event.EventType == EET_KEY_INPUT_EVENT)
	{
		if(event.KeyInput.PressedDown)
		{
			if(event.KeyInput.Key == KEY_ESCAPE)
			{
				irrlichtBase.changeGameState("MainMenu");
			}
		}
	}

	if(event.EventType == EET_MOUSE_INPUT_EVENT)
	{
		position2d<int> mouse = position2d<int>(event.MouseInput.X, event.MouseInput.Y);

		if(event.MouseInput.Event == EMIE_MOUSE_MOVED)
		{
			game->proceedInput(event);
		}

		if(event.MouseInput.Event == EMIE_LMOUSE_PRESSED_DOWN)
		{
			game->proceedInput(event);
		}

		if(event.MouseInput.Event == EMIE_RMOUSE_PRESSED_DOWN)
		{
			game->proceedInput(event);
		}
	}

	return(false);
}

//*********************************************************************************/
void Game::update(void)
{
	background->draw();
	
	driver->draw2DImage(transwindow, position2d<int>(0,(irrlichtBase.getHeight()-320)/2), rect<int>(0,0,176,320), 0, SColor(255,255,255,255), true);
	driver->draw2DImage(transwindow, position2d<int>(irrlichtBase.getWidth()-176,(irrlichtBase.getHeight()-320)/2), rect<int>(0,0,176,320), 0, SColor(255,255,255,255), true);

	game->update();
	game->render();

	for(unsigned int x = 0; x < GAME_FIELD_X; x++)
	{
		for(unsigned int y = 0; y < GAME_FIELD_Y; y++)
		{
			driver->draw2DImage(pattern, position2d<int>(64*x + (irrlichtBase.getWidth()-64*GAME_FIELD_X)/2, 64*y + (irrlichtBase.getHeight()-64*GAME_FIELD_Y)/2), rect<int>(0,0,64,64), 0, SColor(255,255,255,255), true);
		}
	}

	cout << game->isWinner() << endl;
	if(game->isWinner() == 1)
	{
		message->showScreenMessage(settings.player1Name + stringc("won the match"), 16, 2000);
	}
	else if(game->isWinner() == 2)
	{
		message->showScreenMessage(settings.player2Name + stringc("won the match"), 16, 2000);
	}

	if(message->isRunning())
	{
		cout << "hkdjc" << endl;
		message->render();
	}
}