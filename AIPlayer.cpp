/**********************************************************************************

Copyright (C) 2009  RPdev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

//*********************************************************************************/

//*********************************************************************************/
#include "AIPlayer.h"

//*********************************************************************************/
AIPlayer::AIPlayer(void)
{

}

//*********************************************************************************/
AIPlayer::~AIPlayer(void)
{

}

//*********************************************************************************/
void AIPlayer::setCurrentField(IField* field)
{
	current = field;
}

//*********************************************************************************/
int AIPlayer::getRowOfTrust(int player, unsigned int maxDepth, IField* cField)
{
	srand(time(0));
	
	setCurrentField(cField);
	playerID = player;
	
	int eval = getMax(maxDepth, current, player);

	if(eval == 4)
	{
		index = rand()%7;
	}

	return(index);
}

//*********************************************************************************/
int AIPlayer::getMax(int depth, IField* currentField, int player)
{
	int evaluationValue = 0, eval;
	evaluationValue = -10000;
	int counter = 0;

	for(unsigned int i = 0; i < GAME_FIELD_X; i++)
	{
		IField* temp = new Field;
		temp->setFieldTo(currentField->getField());
		
		if(temp->isTurnPossible(i))
		{
			temp->doTurn(i, player);

			if(depth <= 1)
			{
				eval = getEvaluation(temp, playerID);
			}
			else
			{
				eval = getMin(depth-1, temp, player%2+1);
			}

			if(eval > evaluationValue)
			{
				evaluationValue = eval;
				index = i;
			}
		}
		else
		{
			
		}			
	}

	return(evaluationValue);
}

//*********************************************************************************/
int AIPlayer::getMin(int depth, IField* currentField, int player)
{
	int evaluationValue = 0, eval;
	evaluationValue = 10000;

	for(unsigned int i = 0; i < GAME_FIELD_X; i++)
	{
		IField* temp = new Field;
		temp->setFieldTo(currentField->getField());
		
		if(temp->isTurnPossible(i))
		{
			temp->doTurn(i, player);

			if(depth <= 1)
			{
				eval = getEvaluation(temp, playerID);
			}
			else
			{
				eval = getMax(depth-1, temp, player%2+1);
			}

			if(eval < evaluationValue)
			{
				evaluationValue = eval;
				index = i;
			}
		}
		else
		{

		}

		delete temp;
	}

	return(evaluationValue);
}

//*********************************************************************************/
int AIPlayer::getEvaluation(IField* field, int player)
{
	int theOther = player%2+1;
	
	if(field->isWinner() == player)
	{
		return(8);
	}
	else if(field->isWinner() == theOther)
	{
		return(0);
	}
	else if(field->threeInARowWin(player))
	{
		return(7);
	}
	else if(field->threeInARowWin(theOther))
	{
		return(1);
	}
	else if(field->threeInARow(player))
	{
		return(6);
	}
	else if(field->threeInARow(theOther))
	{
		return(2);
	}
	else if(field->twoInARow(player))
	{
		return(5);
	}
	else if(field->twoInARow(theOther))
	{
		return(3);
	}
	
	return(4);
}