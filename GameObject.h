/**********************************************************************************

Copyright (C) 2009  RPdev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

//*********************************************************************************/

//*********************************************************************************/
#if !defined(__gameobject_h__)
#define __gameobject_h__

//*********************************************************************************/
#include "interface.h"
#include "Player.h"
#include "defines.h"
#include "Field.h"
#include "GameSettings.h"
#include "../GameCreator/IrrlichtBase.h"
#include "AIPlayer.h"

//*********************************************************************************/
class GameObject : public IGameObject
{
protected:
	IPlayer* player[2];
	int activePlayer;
	IField* field;
	ITexture* player1, *player2;
	int numOfChips;

	ITexture* forecastChip;
	position2d<int> forecastChipPosition;

public:
	GameObject(void);
	virtual ~GameObject(void);

	void initialize(void);
	void shutdown(void);

	virtual void newRound(void);
	virtual int isWinner(void);

	virtual bool proceedInput(const SEvent &event);
	virtual void OnGameEvent(GameEvent* event);

	void update(void);
	void render(void);
};

//*********************************************************************************/
#endif