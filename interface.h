/**********************************************************************************

Copyright (C) 2009  RPdev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

//*********************************************************************************/

//***********************************************************************************//
#if !defined(__gameinterface_h__)
#define __gameinterface_h__

//***********************************************************************************//
#include <iostream>
#include <vector>
#include <time.h>
#include <map>
using namespace std;

#include <irrlicht.h>

#pragma comment(lib, "Irrlicht.lib")

using namespace irr;

using namespace core;
using namespace scene;
using namespace video;
using namespace gui;
using namespace io;


#include "defines.h"

//***********************************************************************************//
enum GAME_EVENT_TYPE
{
	CHIP_SET = 0,
	CHIP_STEAL,
	PLAYER_WON,
	AI_CHIP_SET,

	NUM_OF_TYPES
};

//***********************************************************************************//
struct GameEvent
{
	struct ChipSetEvent
	{
		int x,y;
		int player;
	};

	struct PlayerWonEvent
	{
		int player;
	};

	struct ChipStealEvent
	{
		int x,y;
	};

	struct AIChipSetEvent
	{
		int player;
		int depth;
	};

	GAME_EVENT_TYPE type;

	union 
	{
		struct ChipSetEvent chipSetEvent;
		struct PlayerWonEvent playerWonEvent;
		struct ChipStealEvent chipStealEvent;
		struct AIChipSetEvent aiChipSetEvent;
	};
};

//***********************************************************************************//
class IGameObject
{
public:
	virtual void initialize(void) = 0;
	virtual void shutdown(void) = 0;
	virtual void newRound(void) = 0;
	virtual bool proceedInput(const SEvent &event) = 0;
	virtual void OnGameEvent(GameEvent* event) = 0;
	virtual void update(void) = 0;
	virtual int isWinner(void) = 0;
	virtual void render(void) = 0;
};

//***********************************************************************************//
class IField
{
public:
	virtual void initialize(void) = 0;
	virtual void shutdown(void) = 0;
	virtual vector<int> getField(void) = 0;
	virtual void setFieldTo(vector<int> copy) = 0;
	virtual position2d<int> doTurn(int row, int player) = 0;
	virtual bool isTurnPossible(int row) = 0;
	virtual void reduceWinnerChips(void) = 0;
	virtual int getPlayer(int indexX, int indexY) = 0;
	virtual void stealChip(int indexX, int indexY) = 0;
	virtual int isWinner(void) = 0;
	virtual bool threeInARowWin(int player) = 0;
	virtual bool threeInARow(int player) = 0;
	virtual bool twoInARow(int player) = 0;
};

//***********************************************************************************//
class IPlayer
{
public:
	virtual void initialize(void) = 0;
	virtual void shutdown(void) = 0;
	virtual void newRound(void) = 0;
	virtual void setName(stringc newName) = 0;
	virtual void setColor(ITexture* newColor) = 0;
	virtual void setField(IField* current) = 0;
	virtual void setCPUControlled(bool cpuControlled) = 0;
	virtual stringc getName(void) = 0;
	virtual ITexture* getColor(void) = 0;
	virtual int getID(void) = 0;
	virtual bool isCPUControlled(void) = 0;
	virtual void noticeTurn(position2d<int> indicies, int player) = 0;
	virtual void win(void) = 0;
	virtual void loose(void) = 0;
	virtual void render(void) = 0;
};

//*********************************************************************************/
struct Move
{
	IField* field;
	int depth;
	int evaluation;
	Move* childs[GAME_FIELD_X];
	Move* parent;

	int row;
};

//*********************************************************************************/
class IAIPlayer
{
public:
	virtual void setCurrentField(IField* field) = 0;
	virtual int getRowOfTrust(int player, unsigned int maxDepth, IField* cField) = 0;
	virtual int getEvaluation(IField* field, int player) = 0;
};

//*********************************************************************************/
struct MinButton
{
	unsigned int id;
	position2d<int> position;
	ITexture* image;
	ITexture* imageHover;
	bool isHovered;

	void getHoverInformation(position2d<int> mouse)
	{
		isHovered = false;

		if(rect<int>(position, image->getOriginalSize()).isPointInside(mouse))
		{
			isHovered = true;
		}
	}

	ITexture* getImage(void)
	{
		if(isHovered)
		{
			return(imageHover);
		}
		else
		{
			return(image);
		}
	}
};

//***********************************************************************************//
#endif
