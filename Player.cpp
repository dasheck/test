/**********************************************************************************

Copyright (C) 2009  RPdev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

//*********************************************************************************/

//*********************************************************************************/
#include "Player.h"

//*********************************************************************************/
unsigned int Player::id = 0;

//*********************************************************************************/
Player::Player(void)
{
	id++;
	personalID = id;
}

//*********************************************************************************/
Player::~Player(void)
{
	
}

//*********************************************************************************/
void Player::initialize(void)
{
	wonGames = 0;
	lostGames = 0;
	name = "Player";
	name += stringc(id);
	ai = false;

	for(unsigned int x = 0; x < GAME_FIELD_X; x ++)
	{
		for(unsigned int y= 0; y < GAME_FIELD_Y; y ++)
		{
			field[x][y] = 0;
		}
	}
	
	text[0] = guidev->addStaticText(stringw(name).c_str(), rect<int>((personalID-1)*(irrlichtBase.getWidth()-176),(irrlichtBase.getHeight()-320)/2+16,(personalID-1)*(irrlichtBase.getWidth()-176)+176,(irrlichtBase.getHeight()-320)/2+44));
	text[1] = guidev->addStaticText(tagW("application.game.won"), rect<int>((personalID-1)*(irrlichtBase.getWidth()-176),(irrlichtBase.getHeight()-320)/2+60,(personalID-1)*(irrlichtBase.getWidth()-176)+176,(irrlichtBase.getHeight()-320)/2+84));
	text[2] = guidev->addStaticText(L"0", rect<int>((personalID-1)*(irrlichtBase.getWidth()-176),(irrlichtBase.getHeight()-320)/2+94,(personalID-1)*(irrlichtBase.getWidth()-176)+176,(irrlichtBase.getHeight()-320)/2+118));
	text[3] = guidev->addStaticText(tagW("application.game.lost"), rect<int>((personalID-1)*(irrlichtBase.getWidth()-176),(irrlichtBase.getHeight()-320)/2+138,(personalID-1)*(irrlichtBase.getWidth()-176)+176,(irrlichtBase.getHeight()-320)/2+162));
	text[4] = guidev->addStaticText(L"0", rect<int>((personalID-1)*(irrlichtBase.getWidth()-176),(irrlichtBase.getHeight()-320)/2+172,(personalID-1)*(irrlichtBase.getWidth()-176)+176,(irrlichtBase.getHeight()-320)/2+196));

	for(unsigned int i = 0; i < 5; i ++)
	{
		text[i]->setOverrideColor(SColor(255,255,255,255));
		text[i]->setTextAlignment(EGUIA_CENTER, EGUIA_CENTER);
	}
}

//*********************************************************************************/
void Player::shutdown(void)
{
	id--;
}

//************************************************************************/
void Player::newRound(void)
{
	for(unsigned int x = 0; x < GAME_FIELD_X; x ++)
	{
		for(unsigned int y= 0; y < GAME_FIELD_Y; y ++)
		{
			field[x][y] = 0;
		}
	}
}

//*********************************************************************************/
void Player::setName(stringc newName)
{
	name = newName;
	text[0]->setText(stringw(name).c_str());
}

//*********************************************************************************/
void Player::setColor(ITexture* newColor)
{
	color = newColor;
}

//*********************************************************************************/
void Player::setField(IField* current)
{
	vector<int> temp = current->getField();

	for(unsigned int x = 0; x < GAME_FIELD_X; x ++)
	{
		for(unsigned int y = 0; y < GAME_FIELD_Y; y ++)
		{
			field[x][y] = temp[y*GAME_FIELD_X+x];
		}
	}
}

//*********************************************************************************/
void Player::setCPUControlled(bool cpuControlled)
{
	ai = cpuControlled;
}

//*********************************************************************************/
stringc Player::getName(void)
{
	return(name);
}

//*********************************************************************************/
ITexture* Player::getColor(void)
{
	return(color);
}

//*********************************************************************************/
int Player::getID(void)
{
	return(personalID);
}

//*********************************************************************************/
bool Player::isCPUControlled(void)
{
	return(ai);
}

//*********************************************************************************/
void Player::noticeTurn(position2d<int> indicies, int player)
{
	field[indicies.X][indicies.Y] = player;
}

//*********************************************************************************/
void Player::win(void)
{
	wonGames++;
	text[2]->setText(stringw(wonGames).c_str());
}

//*********************************************************************************/
void Player::loose(void)
{
	lostGames++;
	text[4]->setText(stringw(lostGames).c_str());
}

//*********************************************************************************/
void Player::render(void)
{
	for(unsigned int x = 0; x < GAME_FIELD_X; x ++)
	{
		for(unsigned int y= 0; y < GAME_FIELD_Y; y ++)
		{
			if(field[x][y] == personalID)
			{
				driver->draw2DImage(color, position2d<int>(64*x + (irrlichtBase.getWidth() - 64*GAME_FIELD_X)/2, 
														   64*y + (irrlichtBase.getHeight()-64*GAME_FIELD_Y)/2));
			}
		}
	}

	for(unsigned int i = 0; i < 5; i ++)
	{
		text[i]->draw();
	}
}
