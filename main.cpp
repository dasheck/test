/**********************************************************************************

Copyright (C) 2009  RPdev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

//*********************************************************************************/

#include "../GameCreator/IrrlichtBase.h"
#include "../GameCreator/defines.h"
#include "../GameCreator/utility.h"
#include "Intro.h"
#include "MainMenu.h"
#include "Game.h"
#include "Options.h"

#include "../GameCreator/InfoBox.h"

//*********************************************************************************/
int main(void)
{
	if(!irrlichtBase.initializeFromFile(CONCAT(FSETTINGS, "settings.xml")))
	{
		irrlichtBase.initialize();
	}	

	irrlichtBase.setFont(CONCAT(FSYSTEM, "rmg2000_16.xml"));
	resource->addDirectory(FIMAGE);

	settings.player1Name = tag("application.option.game.name1");
	settings.player2Name = tag("application.option.game.name2");
	settings.gamemode = 0;

	IGameState* intro = new Intro;
	IGameState* mainmenu = new MainMenu;
	IGameState* game = new Game;
	IGameState* options = new Options;

	irrlichtBase.addGameState(intro, true);
	irrlichtBase.addGameState(mainmenu);
	irrlichtBase.addGameState(game);
	irrlichtBase.addGameState(options);

	device->setWindowCaption(tagW("application.title"));

	IInfoBox* box = new InfoBox;
	box->initialize();
	box->showMessage(tagC("application.title"),dimension2d<unsigned int>(200,50), 8000, Enumerations::IA_MIDDLE, Enumerations::IA_MIDDLE);

	while(device->run())
	{
		device->getVideoDriver()->beginScene(true,true,SColor(255,255,255,255));

			irrlichtBase.render();
			box->render();

		device->getVideoDriver()->endScene();
	}

	irrlichtBase.finalize();

	return(0);
}