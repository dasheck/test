/**********************************************************************************

Copyright (C) 2009  RPdev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

//*********************************************************************************/
#include "Field.h"

//*********************************************************************************/
Field::Field(void)
{

}

//*********************************************************************************/
Field::~Field(void)
{

}

//*********************************************************************************/
void Field::initialize(void)
{
	for(unsigned int x = 0; x < GAME_FIELD_X; x ++)
	{
		for(unsigned int y= 0; y < GAME_FIELD_Y; y ++)
		{
			field[x][y] = 0;
		}
	}
}

//*********************************************************************************/
void Field::shutdown(void)
{

}

//*********************************************************************************/
vector<int> Field::getField(void)
{
	vector<int> temp;

	for(unsigned int y= 0; y < GAME_FIELD_Y; y ++)
	{
		for(unsigned int x = 0; x < GAME_FIELD_X; x ++)
		{
			temp.push_back(field[x][y]);
		}
	}

	return(temp);
}

//*********************************************************************************/
void Field::setFieldTo(vector<int> copy)
{
	for(unsigned int x = 0; x < GAME_FIELD_X; x ++)
	{
		for(unsigned int y = 0; y < GAME_FIELD_Y; y ++)
		{
			field[x][y] = copy[y*GAME_FIELD_X+x];
		}
	}
}

//*********************************************************************************/
position2d<int> Field::doTurn(int row, int player)
{
	for(int i = GAME_FIELD_Y-1; i >= 0; i--)
	{
		if(field[row][i] == 0)
		{
			field[row][i] = player;
			return(position2d<int>(row,i));
		}
	}

	return(position2d<int>(row,-1));
}

//*********************************************************************************/
bool Field::isTurnPossible(int row)
{
	if(field[row][0] == 0)
	{
		return(true);
	}

	return(false);
}

//*********************************************************************************/
int Field::getPlayer(int indexX, int indexY)
{
	return(field[indexX][indexY]);
}

//*********************************************************************************/
void Field::stealChip(int indexX, int indexY)
{
	for(unsigned int y = indexY; y > 0; y --)
	{
		field[indexX][y] = field[indexX][y-1];
	}
}

//*********************************************************************************/
void Field::reduceWinnerChips(void)
{
	//waagerecht
	for(int y = 0; y < GAME_FIELD_Y; y ++)
		for(int x = 0; x < GAME_FIELD_X - 3; x ++)
		{
			if((field[x+1][y] == field[x][y] && 
				field[x+2][y] == field[x][y] && 
				field[x+3][y] == field[x][y]) && field[x][y] != 0)
			{
				if(y != 0)
				{
					for(unsigned int xs = x; xs < x+4; xs++)
					{
						for(unsigned int ys = y; ys > 0; ys--)
						{
							field[xs][ys] = field[xs][ys-1];
						}
					}
				}
				else
				{
					for(unsigned int xs = x; xs < x+4; xs++)
					{
						field[xs][y] = 0;
					}
				}			
			}
		}

	//senkrecht
	for(int x = 0; x < GAME_FIELD_X; x ++)
		for(int y = 0; y < GAME_FIELD_Y - 3; y ++)
		{
			if((field[x][y+1] == field[x][y] &&
				field[x][y+2] == field[x][y] && 
				field[x][y+3] == field[x][y]) && field[x][y] != 0)
			{
				for(unsigned int ys = y; ys < y+4; ys++)
				{
					field[x][ys] = 0;
				}
			}
		}

	//dann die linksdiagonalen
	for(int x = 0; x < GAME_FIELD_X - 3; x ++)
		for(int y = 0; y < GAME_FIELD_Y - 3; y ++)
		{
			if((field[x+1][y+1] == field[x][y] &&
				field[x+2][y+2] == field[x][y] && 
				field[x+3][y+3] == field[x][y]) && field[x][y] != 0)
			{
				if(y != 0)
				{
					for(unsigned int xs = x; xs < x+4; xs++)
					{
						int ys = y+xs-x;

						while(ys > 0)
						{
							field[xs][ys] = field[xs][ys-1];
							ys--;
						}
					}
				}

				for(unsigned int xs = x; xs < x+4; xs++)
				{
					field[xs][0] = 0;
				}
			}
		}

	//dann die rechtsdiagonalen
	for(int x = 3; x < GAME_FIELD_X; x ++)
		for(int y = 0; y < GAME_FIELD_Y - 3; y ++)
		{
			if((field[x-1][y+1] == field[x][y] &&
				field[x-2][y+2] == field[x][y] && 
				field[x-3][y+3] == field[x][y]) && field[x][y] != 0)
			{
				if(y != 0)
				{
					for(unsigned int xs = x; xs > x-4; xs--)
					{
						int ys = y+x-xs;

						while(ys > 0)
						{
							field[xs][ys] = field[xs][ys-1];
							ys--;
						}
					}
				}

				for(unsigned int xs = x; xs > x-4; xs--)
				{
					field[xs][0] = 0;
				}
			}
		}
}

//*********************************************************************************/
int Field::isWinner(void)
{
	//waagerecht
	for(int y = 0; y < GAME_FIELD_Y; y ++)
		for(int x = 0; x < GAME_FIELD_X - 3; x ++)
		{
			if((field[x+1][y] == field[x][y] && 
			   field[x+2][y] == field[x][y] && 
			   field[x+3][y] == field[x][y]) && field[x][y] != 0)
			{
				return(field[x][y]);
			}
		}

	//senkrecht
	for(int x = 0; x < GAME_FIELD_X; x ++)
		for(int y = 0; y < GAME_FIELD_Y - 3; y ++)
		{
			if((field[x][y+1] == field[x][y] &&
			   field[x][y+2] == field[x][y] && 
			   field[x][y+3] == field[x][y]) && field[x][y] != 0)
			{
				return(field[x][y]);
			}
		}

	//dann die linksdiagonalen
	for(int x = 0; x < GAME_FIELD_X - 3; x ++)
		for(int y = 0; y < GAME_FIELD_Y - 3; y ++)
		{
			if((field[x+1][y+1] == field[x][y] &&
			   field[x+2][y+2] == field[x][y] && 
			   field[x+3][y+3] == field[x][y]) && field[x][y] != 0)
			{
				return(field[x][y]);
			}
		}

	//dann die rechtsdiagonalen
	for(int x = 3; x < GAME_FIELD_X; x ++)
		for(int y = 0; y < GAME_FIELD_Y - 3; y ++)
		{
			if((field[x-1][y+1] == field[x][y] &&
			    field[x-2][y+2] == field[x][y] && 
			    field[x-3][y+3] == field[x][y]) && field[x][y] != 0)
			{
				return(field[x][y]);
			}
		}

	return(0);
}

//*********************************************************************************/
bool Field::threeInARowWin(int player)
{
	bool flag = false;
	
	//waagerecht
	for(int y = 0; y < GAME_FIELD_Y; y ++)
		for(int x = 1; x < GAME_FIELD_X - 3; x ++)
		{
			if((field[x+1][y] == field[x][y] && 
				field[x+2][y] == field[x][y]) && field[x][y] == player)
			{
				if(field[x-1][y] == 0 && field[x+3][y] == 0)
				{
					if(y == GAME_FIELD_Y - 1)
					{
						flag = true;
					}
					else
					{
						if(field[x-1][y+1] != 0 && field[x+3][y+1] != 0)
						{
							flag = true;
						}
					}
				}				
			}
		}

	if(flag)
	{
		return(flag);
	}

	//linksdiagonal
	for(int x = 1; x < GAME_FIELD_X - 4; x ++)
		for(int y = 1; y < GAME_FIELD_Y - 3; y ++)
		{
			if((field[x+1][y+1] == field[x][y] &&
				field[x+2][y+2] == field[x][y]) && field[x][y] == player)
			{
				if(field[x-1][y-1] == 0 && field[x+3][y+3] == 0)
				{
					if(y == GAME_FIELD_Y - 4)
					{
						if(field[x-1][y] != 0)
						{
							flag = true;
						}
					}
					else
					{
						if(field[x-1][y] != 0 && field[x+3][y+4] != 0)
						{
							flag = true;
						}
					}
				}
			}
		}

	if(flag)
	{
		return(flag);
	}

	//rechtsdiagonal
	for(int x = 3; x < GAME_FIELD_X - 1; x ++)
		for(int y = 1; y < GAME_FIELD_Y - 3; y ++)
		{
			if((field[x-1][y+1] == field[x][y] &&
				field[x-2][y+2] == field[x][y]) && field[x][y] == player)
			{
				if(field[x+1][y-1] == 0 && field[x+-3][y+3] == 0)
				{
					if(y == GAME_FIELD_Y - 4)
					{
						if(field[x+1][y] != 0)
						{
							flag = true;
						}
					}
					else
					{
						if(field[x+1][y] != 0 && field[x-3][y+4] != 0)
						{
							flag = true;
						}
					}
				}
			}
		}
	
	return(flag);
}

//*********************************************************************************/
bool Field::threeInARow(int player)
{
	bool flag = false;
	
	//waagerecht
	for(int y = 0; y < GAME_FIELD_Y; y ++)
		for(int x = 0; x < GAME_FIELD_X - 2; x ++)
		{
			if((field[x+1][y] == field[x][y] && 
				field[x+2][y] == field[x][y]) && field[x][y] == player)
			{
				if(y == GAME_FIELD_Y-1)
				{
					if(x == 0)
					{
						if(field[x+3][y] == 0)
						{
							flag = true;
						}
					}
					else if(x == GAME_FIELD_X-3)
					{
						if(field[x-1][y] == 0)
						{
							flag = true;
						}
					}
					else
					{
						if(field[x-1][y] == 0 || field[x+3][y] == 0)
						{
							flag = true;
						}
					}
				}
				else
				{
					if(x == 0)
					{
						if(field[x+3][y] == 0)
						{
							flag = true;
						}
					}
					else if(x == GAME_FIELD_X-3)
					{
						if(field[x-1][y] == 0)
						{
							flag = true;
						}
					}
					else
					{
						if((field[x-1][y+1] != 0 && field[x-1][y] == 0) || 
						   (field[x+3][y+1] != 0 && field[x+3][y] == 0))
						{
							flag = true;
						}
					}
				}
			}
		}

		if(flag)
		{
			return(flag);
		}

		//senkrecht
		for(int x = 0; x < GAME_FIELD_X; x ++)
			for(int y = 1; y < GAME_FIELD_Y - 2; y ++)
			{
				if((field[x][y+1] == field[x][y] &&
					field[x][y+2] == field[x][y]) && field[x][y] == player)
				{
					if(field[x][y-1] == 0)
					{
						flag = true;
					}
				}
			}

		//linksdiagonal

	for(int x = 0; x < GAME_FIELD_X - 2; x ++)
			for(int y = 0; y < GAME_FIELD_Y - 2; y ++)
			{
				if((field[x+1][y+1] == field[x][y] &&
					field[x+2][y+2] == field[x][y]) && field[x][y] == player)
				{
					if(y == GAME_FIELD_Y - 3)
					{
						if(x > 0)
						{	
							if(field[x-1][y-1] == 0 && field[x-1][y] != 0)
							{
								flag = true;
							}
						}
					}
					else if(y == 0)
					{
						if(x < GAME_FIELD_X-3)
						{
							if(field[x+3][y+3] == 0 && field[x+3][y+4] != 0)
							{
								flag = true;
							}
						}
					}
					else
					{
						if(x == 0)
						{
							if(y == GAME_FIELD_Y-4)
							{
								if(field[x+3][y+3] == 0)
								{
									flag = true;
								}
							}
							else
							{
								if(field[x+3][y+3] == 0 && field[x+3][y+4] != 0)
								{
									flag = true;
								}
							}
						}
						else if(x == GAME_FIELD_X-3)
						{
							if(field[x-1][y-1] == 0 && field[x-1][y] != 0)
							{
								flag = true;
							}
						}
						else
						{
							if((field[x+3][y+3] == 0 && field[x+3][y+4] != 0) ||
							   (field[x-1][y-1] == 0 && field[x-1][y] != 0))
							{
								flag = true;
							}
						}
					}
				}
			}

			if(flag)
			{
				return(flag);
			}

			//rechtsdiagonal
			for(int x = 2; x < GAME_FIELD_X; x ++)
				for(int y = 0; y < GAME_FIELD_Y - 2; y ++)
				{
					if((field[x-1][y+1] == field[x][y] &&
						field[x-2][y+2] == field[x][y]) && field[x][y] == player)
					{
						if(y == GAME_FIELD_Y - 3)
						{
							if(x < GAME_FIELD_X-1)
							{	
								if(field[x+1][y-1] == 0 && field[x+1][y] != 0)
								{
									flag = true;
								}
							}
						}
						else if(y == 0)
						{
							if(x > 2)
							{
								if(field[x-3][y+3] == 0 && field[x-3][y+4] != 0)
								{
									flag = true;
								}
							}
						}
						else
						{
							if(x == GAME_FIELD_X-1)
							{
								if(y == GAME_FIELD_Y-4)
								{
									if(field[x-3][y+3] == 0)
									{
										flag = true;
									}
								}
								else
								{
									if(field[x-3][y+3] == 0 && field[x-3][y+4] != 0)
									{
										flag = true;
									}
								}
							}
							else if(x == 2)
							{
								if(field[x+1][y-1] == 0 && field[x+1][y] != 0)
								{
									flag = true;
								}
							}
							else
							{
								if((field[x-3][y+3] == 0 && field[x-3][y+4] != 0) ||
									(field[x+1][y-1] == 0 && field[x+1][y] != 0))
								{
									flag = true;
								}
							}
						}
					}
				}

				if(flag)
				{
					return(flag);
				}

	return(flag);
}

//*********************************************************************************/
bool Field::twoInARow(int player)
{
	bool flag = false;

	return(flag);
}