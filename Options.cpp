/**********************************************************************************

Copyright (C) 2009  RPdev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

//*********************************************************************************/

//*********************************************************************************/
#include "Options.h"

//*********************************************************************************/
Options::Options(void)
{
	name = "Options";
}

//*********************************************************************************/
Options::~Options(void)
{

}

//*********************************************************************************/
void Options::OnEnter(void)
{
	if(firstEnter)
	{
		background = guidev->addImage(rect<int>(0,0,irrlichtBase.getWidth(),irrlichtBase.getHeight()));
		background->setScaleImage(true);
		background->setImage(resource->getItem("ibackground"));

		firstEnter = false;

		root = guidev->addTabControl(rect<int>(32,32,irrlichtBase.getWidth()-32, irrlichtBase.getHeight()-32));
		game = root->addTab(tagW("application.option.game.title"));
		game->setBackgroundColor(SColor(128,255,255,255));
		game->setDrawBackground(true);
		fastGame = guidev->addButton(rect<int>((irrlichtBase.getWidth()- 640)/2-32,16,(irrlichtBase.getWidth()-640)/2+640-32,112), game, 1);
		fastGame->setImage(resource->getItem("bschnellesspiel"));
		tetrisGame = guidev->addButton(rect<int>((irrlichtBase.getWidth()- 640)/2-32,128,(irrlichtBase.getWidth()-640)/2+640-32,224), game, 2);
		tetrisGame->setImage(resource->getItem("btetris_disable"));
		sunshineGame = guidev->addButton(rect<int>((irrlichtBase.getWidth()- 640)/2-32,240,(irrlichtBase.getWidth()-640)/2+640-32,336), game, 3);
		sunshineGame->setImage(resource->getItem("bsunshine_disable"));
		chosenButton = 1;
		desc[0] = tagW("application.option.game.desc1");
		desc[1] = tagW("application.option.game.desc2");
		desc[2] = tagW("application.option.game.desc3");
		gameDescription = guidev->addStaticText(desc[0].c_str(),
			rect<int>((irrlichtBase.getWidth()- 640)/2-32+410, 352, (irrlichtBase.getWidth()- 640)/2-32+640, irrlichtBase.getHeight()-112),false,true,game);
		gameDescription->setOverrideColor(SColor(255,0,0,0));
		gameDescription->setTextAlignment(EGUIA_CENTER, EGUIA_UPPERLEFT);
		guidev->addStaticText(tagW("application.option.game.namelabel"), rect<int>((irrlichtBase.getWidth()- 640)/2-32, 352, (irrlichtBase.getWidth()- 640)/2-32+100, 382),false,true,game);
		player1Name = guidev->addEditBox(tagW("application.option.game.name1"), rect<int>((irrlichtBase.getWidth()- 640)/2-32+100, 352, (irrlichtBase.getWidth()- 640)/2-32+250, 382), false, game);
		player1Mode = guidev->addCheckBox(false,rect<int>((irrlichtBase.getWidth()- 640)/2-32+260, 352, (irrlichtBase.getWidth()- 640)/2-32+400, 382),game,4,tagW("application.option.game.ai"));
		guidev->addStaticText(tagW("application.option.game.namelabel"), rect<int>((irrlichtBase.getWidth()- 640)/2-32, 398, (irrlichtBase.getWidth()- 640)/2-32+100, 428),false,true,game);
		player2Name = guidev->addEditBox(tagW("application.option.game.name2"), rect<int>((irrlichtBase.getWidth()- 640)/2-32+100, 398, (irrlichtBase.getWidth()- 640)/2-32+250, 428), false, game);
		player2Mode = guidev->addCheckBox(false,rect<int>((irrlichtBase.getWidth()- 640)/2-32+260, 398, (irrlichtBase.getWidth()- 640)/2-32+400, 428),game,5,tagW("application.option.game.ai"));
		player1Mode->setEnabled(false);
		player2Mode->setEnabled(false);
		
		ApplicationSettings appSettings = irrlichtBase.getApplicationSettings();

		video = root->addTab(tagW("application.option.video.title"));
		video->setBackgroundColor(SColor(128,255,255,255));
		video->setDrawBackground(true);
		guidev->addStaticText(tagW("application.option.video.resolution"), rect<int>((irrlichtBase.getWidth()- 640)/2-32, 16, (irrlichtBase.getWidth()- 640)/2-32+200, 38),false,true,video);
		resolution = guidev->addComboBox(rect<int>((irrlichtBase.getWidth()- 640)/2-32, 54, (irrlichtBase.getWidth()- 640)/2-32+200, 76), video, 6);
		resolution->addItem(tagW("application.option.video.res1"));
		resolution->addItem(tagW("application.option.video.res2"));
		resolution->addItem(tagW("application.option.video.res3"));
		
		switch(appSettings.width)
		{
			case 800:
				resolution->setSelected(0);
				break;
		
			case 1024:
				resolution->setSelected(1);
				break;

			case 1280:
				resolution->setSelected(2);
				break;
		}

		fullscreen = guidev->addCheckBox(appSettings.fullscreen, rect<int>((irrlichtBase.getWidth()- 640)/2-32, 92, (irrlichtBase.getWidth()- 640)/2-32+200, 114), video, 7, tagW("application.option.video.fullscreen"));
		guidev->addStaticText(tagW("application.option.video.language"), rect<int>((irrlichtBase.getWidth()- 640)/2-32, 130, (irrlichtBase.getWidth()- 640)/2-32+200, 152),false,true,video);
		language = guidev->addComboBox(rect<int>((irrlichtBase.getWidth()- 640)/2-32, 168, (irrlichtBase.getWidth()- 640)/2-32+200, 190), video, 8);
		language->addItem(tagW("application.option.video.lang1"));
		language->addItem(tagW("application.option.video.lang2"));

		if(appSettings.language == "german")
		{
			language->setSelected(0);
		}
		else if(appSettings.language == "english")
		{
			language->setSelected(1);
		}

	//	audio = root->addTab(tagW("application.option.audio.title"));
	//	audio->setBackgroundColor(SColor(128,255,255,255));
	//	audio->setDrawBackground(true);
		highscore = root->addTab(tagW("application.option.highscore.title"));
		highscore->setBackgroundColor(SColor(128,255,255,255));
		highscore->setDrawBackground(true);
		credits = root->addTab(tagW("application.option.credits.title"));
		credits->setBackgroundColor(SColor(128,255,255,255));
		credits->setDrawBackground(true);
	}
}

//*********************************************************************************/
void Options::OnLeave(void)
{
	settings.player1Name = stringc(player1Name->getText());
	settings.player2Name = stringc(player2Name->getText());
	settings.gamemode = chosenButton;

	ApplicationSettings appSettings;
	
	switch(resolution->getSelected())
	{
		case 0:
			appSettings.width = 800;
			appSettings.height = 600;
			break;

		case 1:
			appSettings.width = 1024;
			appSettings.height = 768;
			break;

		case 2:
			appSettings.width = 1280;
			appSettings.height = 1024;
			break;

		default:
			appSettings.width = 800;
			appSettings.height = 600;
			break;
	}

	appSettings.fullscreen = fullscreen->isChecked();
	
	switch(language->getSelected())
	{
		case 0:
			appSettings.language = stringc("german");
			break;

		case 1:
			appSettings.language = stringc("english");
			break;

		default:
			appSettings.language = stringc("english");
			break;
	}

	irrlichtBase.setApplicationSettings(appSettings);
}

//*********************************************************************************/
bool Options::OnInput(const SEvent &event)
{
	if(event.EventType == EET_GUI_EVENT)
	{
		unsigned int ID = event.GUIEvent.Caller->getID();

		if(event.GUIEvent.EventType == EGET_BUTTON_CLICKED)
		{
			fastGame->setImage(resource->getItem("bschnellesspiel_disable"));
			tetrisGame->setImage(resource->getItem("btetris_disable"));
			sunshineGame->setImage(resource->getItem("bsunshine_disable"));

			if(ID == 1)
			{
				fastGame->setImage(resource->getItem("bschnellesspiel"));
			}
			else if(ID == 2)
			{
				tetrisGame->setImage(resource->getItem("btetris"));
			}
			else if(ID == 3)
			{
				sunshineGame->setImage(resource->getItem("bsunshine"));
			}

			chosenButton = ID-1;
			gameDescription->setText(desc[chosenButton].c_str());			
		}
	}

	if(event.EventType == EET_KEY_INPUT_EVENT)
	{
		if(event.KeyInput.PressedDown)
		{
			if(event.KeyInput.Key == KEY_ESCAPE)
			{
				irrlichtBase.changeGameState("MainMenu");
			}
		}
	}

	return(false);
}

//*********************************************************************************/
void Options::update(void)
{
	background->draw();
	root->draw();
}