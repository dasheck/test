/**********************************************************************************

Copyright (C) 2009  RPdev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

//************************************************************************/

//************************************************************************/
#include "GameObject.h"

//************************************************************************/
GameObject::GameObject(void)
{

}

//************************************************************************/
GameObject::~GameObject(void)
{

}

//************************************************************************/
void GameObject::initialize(void)
{
	for(unsigned int i = 0; i < 2; i++)
	{
		player[i] = new Player;
		player[i]->initialize();
	}

	player[0]->setColor(resource->getItem("ichip64orange"));
	player[1]->setColor(resource->getItem("ichip64green"));

	player[0]->setName(settings.player1Name);
	player[1]->setName(settings.player2Name);

	player[0]->setCPUControlled(false);
	player[1]->setCPUControlled(false);

	activePlayer = 0;
	forecastChip = resource->getItem("ichip64orange");

	field = new Field;
	field->initialize();

	player1 = resource->getItem("belement1");
	player2 = resource->getItem("belement2");

	numOfChips = 0;
}

//************************************************************************/
void GameObject::shutdown(void)
{
	for(unsigned int i = 0; i < 2; i ++)
	{
		player[i]->shutdown();
	}
}

//************************************************************************/
void GameObject::newRound(void)
{
	field->initialize();
	
	for(unsigned int i = 0; i < 2; i++)
	{
		player[i]->newRound();
	}

	numOfChips = 0;
}

//************************************************************************/
bool GameObject::proceedInput(const SEvent &event)
{
	if(event.EventType == EET_MOUSE_INPUT_EVENT)
	{
		position2d<int> mouse = position2d<int>(event.MouseInput.X, event.MouseInput.Y);

		if(event.MouseInput.Event == EMIE_MOUSE_MOVED)
		{
			if(rect<int>((irrlichtBase.getWidth()-64*GAME_FIELD_X)/2, (irrlichtBase.getHeight()-64*GAME_FIELD_Y)/2,
				(irrlichtBase.getWidth()+64*GAME_FIELD_X)/2, (irrlichtBase.getHeight()+64*GAME_FIELD_Y)/2).isPointInside(mouse))
			{
				forecastChipPosition = position2d<int>(64*((mouse.X - (irrlichtBase.getWidth()-64*GAME_FIELD_X)/2)/64)+(irrlichtBase.getWidth()-64*GAME_FIELD_X)/2, (irrlichtBase.getHeight()-64*GAME_FIELD_Y)/2-32);
				
			}
		}

		if(event.MouseInput.Event == EMIE_LMOUSE_PRESSED_DOWN)
		{
			if(rect<int>((irrlichtBase.getWidth()-64*GAME_FIELD_X)/2, (irrlichtBase.getHeight()-64*GAME_FIELD_Y)/2,
				(irrlichtBase.getWidth()+64*GAME_FIELD_X)/2, (irrlichtBase.getHeight()+64*GAME_FIELD_Y)/2).isPointInside(mouse))
			{
				position2d<int> turn = field->doTurn((mouse.X - (irrlichtBase.getWidth()-64*GAME_FIELD_X)/2)/64, player[activePlayer]->getID());
				GameEvent* temp = new GameEvent;
				temp->type = CHIP_SET;
				temp->chipSetEvent.x = turn.X;
				temp->chipSetEvent.y = turn.Y;
				temp->chipSetEvent.player = player[activePlayer]->getID();
				OnGameEvent(temp);			
			}
		}
	}

	return(false);
}

//************************************************************************/
void GameObject::OnGameEvent(GameEvent* event)
{
	if(event->type == CHIP_SET)
	{
		if(event->chipSetEvent.y != -1)
		{
			numOfChips++;

			for(unsigned int i = 0; i < 2; i++)
			{
				player[i]->noticeTurn(position2d<int>(event->chipSetEvent.x, event->chipSetEvent.y), player[activePlayer]->getID());
			}

			activePlayer++;
			forecastChip = resource->getItem("ichip64green");

			if(activePlayer > 1)
			{
				activePlayer = 0;
				forecastChip = resource->getItem("ichip64orange");
			}

			if(field->isWinner() != 0)
			{
				GameEvent* temp = new GameEvent;
				temp->type = PLAYER_WON;
				temp->playerWonEvent.player = field->isWinner();
				OnGameEvent(temp);
			}
	
			if(numOfChips >= GAME_FIELD_X*GAME_FIELD_Y)
			{
				newRound();
			}
		}	
	}

	if(event->type == PLAYER_WON)
	{
		if(event->playerWonEvent.player == player[0]->getID())
		{
			player[0]->win();
			player[1]->loose();
			newRound();
		}

		if(event->playerWonEvent.player == player[1]->getID())
		{
			player[1]->win();
			player[0]->loose();
			newRound();
		}
	}

	if(event->type == AI_CHIP_SET)
	{
		IAIPlayer* temp = new AIPlayer;
		int row = temp->getRowOfTrust(player[event->aiChipSetEvent.player]->getID(), event->aiChipSetEvent.depth, field);

		position2d<int> turn = field->doTurn(row, player[activePlayer]->getID());

		for(unsigned int i = 0; i < 2; i++)
		{
			player[i]->noticeTurn(turn, player[activePlayer]->getID());
		}

		activePlayer++;
		forecastChip = resource->getItem("ichip64green");

		if(activePlayer > 1)
		{
			activePlayer = 0;
			forecastChip = resource->getItem("ichip64orange");
		}

		if(field->isWinner() != 0)
		{
			GameEvent* temp = new GameEvent;
			temp->type = PLAYER_WON;
			temp->playerWonEvent.player = field->isWinner();
			OnGameEvent(temp);
		}

		if(numOfChips >= GAME_FIELD_X*GAME_FIELD_Y)
		{
			newRound();
		}
	}
}

//************************************************************************/
int GameObject::isWinner(void)
{
	return(field->isWinner());
}

//************************************************************************/
void GameObject::update(void)
{
	if(player[activePlayer]->isCPUControlled())
	{
		GameEvent* event = new GameEvent;
		event->type = AI_CHIP_SET;
		event->aiChipSetEvent.player = activePlayer;
		event->aiChipSetEvent.depth = 5;
		OnGameEvent(event);
	}
}

//************************************************************************/
void GameObject::render(void)
{
	driver->draw2DImage(forecastChip, forecastChipPosition, rect<int>(0,0,64,64), 0, SColor(128,255,255,255), true);

	for(unsigned int i = 0; i < 2; i++)
	{
		player[i]->render();
	}
	
	if(activePlayer == 0)
	{
		driver->draw2DImage(player1, position2d<int>(0,irrlichtBase.getHeight()-64),
							rect<int>(0,0,128,32),0,SColor(255,255,255,255),true);
	}
	else if(activePlayer == 1)
	{
		driver->draw2DImage(player2, position2d<int>(irrlichtBase.getWidth()-128, irrlichtBase.getHeight()-64),
							rect<int>(0,0,128,32),0,SColor(255,255,255,255),true);
	}
}