/**********************************************************************************

Copyright (C) 2009  RPdev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

//************************************************************************/

//************************************************************************/
#include "GameObjectTetris.h"

//************************************************************************/
GameObjectTetris::GameObjectTetris(void)
{

}

//************************************************************************/
GameObjectTetris::~GameObjectTetris(void)
{

}

//************************************************************************/
void GameObjectTetris::newRound(void)
{
	field->reduceWinnerChips();

	for(unsigned int i = 0; i < 2; i++)
	{
		player[i]->setField(field);
	}

	numOfChips = numOfChips - 4;
}

//************************************************************************/
void GameObjectTetris::OnGameEvent(GameEvent* event)
{
	if(event->type == CHIP_SET)
	{
		if(event->chipSetEvent.y != -1)
		{
			numOfChips++;

			for(unsigned int i = 0; i < 2; i++)
			{
				player[i]->noticeTurn(position2d<int>(event->chipSetEvent.x, event->chipSetEvent.y), player[activePlayer]->getID());
			}

			activePlayer++;
			forecastChip = resource->getItem("ichip64green");

			if(activePlayer > 1)
			{
				activePlayer = 0;
				forecastChip = resource->getItem("ichip64orange");
			}

			if(field->isWinner() != 0)
			{
				GameEvent* temp = new GameEvent;
				temp->type = PLAYER_WON;
				temp->playerWonEvent.player = field->isWinner();
				OnGameEvent(temp);
			}

			if(numOfChips >= GAME_FIELD_X*GAME_FIELD_Y)
			{
				newRound();
			}
		}	
	}

	if(event->type == PLAYER_WON)
	{
		if(event->playerWonEvent.player == player[0]->getID())
		{
			player[0]->win();
			player[1]->loose();
			newRound();
		}

		if(event->playerWonEvent.player == player[1]->getID())
		{
			player[1]->win();
			player[0]->loose();
			newRound();
		}
	}
}