/**********************************************************************************

Copyright (C) 2009  RPdev

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

//*********************************************************************************/

//*********************************************************************************/
#if !defined(__player_h__)
#define __player_h__

//*********************************************************************************/
#include "interface.h"
#include "defines.h"
#include "../GameCreator/IrrlichtBase.h"

//*********************************************************************************/
class Player : public IPlayer
{
private:
	static unsigned int id;
	stringc name;
	ITexture* color;
	int wonGames, lostGames;
	int field[GAME_FIELD_X][GAME_FIELD_Y];
	int personalID;
	bool ai;

	IGUIStaticText* text[5];

public:
	Player(void);
	~Player(void);

	void initialize(void);
	void shutdown(void);

	void newRound(void);

	void setName(stringc newName);
	void setColor(ITexture* newColor);
	void setField(IField* current);
	void setCPUControlled(bool cpuControlled);

	stringc getName(void);
	ITexture* getColor(void);
	int getID(void);
	bool isCPUControlled(void);

	void noticeTurn(position2d<int> indicies, int player);

	void win(void);
	void loose(void);

	void render(void);
};

//*********************************************************************************/
#endif